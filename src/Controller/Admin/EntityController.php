<?php

namespace App\Controller\Admin;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EntityController
 * @package App\Controller\Admin
 *
 * @Route("/admin", name="admin_")
 */
class EntityController extends AbstractController
{
    /**
     * Used to access custom service
     * without DI if needed
     *
     * @var ContainerInterface
     */
    protected $globalContainer;

    public function __construct(ContainerInterface $container)
    {
        $this->globalContainer = $container;
    }

    /**
     * @Route("/", name="homepage")
     */
    public function homepage()
    {
        return $this->redirectToRoute('admin_dashboard');
    }

    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function dashboard()
    {
        return $this->render('admin/entity/dashboard.html.twig');
    }

    /**
     * @Route("/list/{entityName}", name="entity_list")
     *
     * @param string $entityName
     * @return Response
     */
    public function list($entityName)
    {
        /** @var EntityRepository $repo */
        $repo = $this->getDoctrine()->getRepository("App\\Entity\\" . $entityName);

        /** @var ArrayCollection $records */
        $records = $repo->createQueryBuilder('entity')
            ->getQuery()
            ->getResult()
        ;

        $entityAdmin = $this->globalContainer->get('App\\Admin\\'. $entityName .'Admin');

        return $this->render('admin/entity/list.html.twig', [
            'records' => $records,
            'entityName' => $entityName,
            'entityAdmin' => $entityAdmin
        ]);
    }

    /**
     * One method for new and edit actions.
     * Because it similar.
     *
     * @Route("/new/{entityName}", name="entity_new")
     * @Route("/edit/{entityName}/{entityId}", name="entity_edit")
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param string $entityName
     * @param mixed $entityId
     *
     * @return Response
     */
    public function form(Request $request, EntityManagerInterface $entityManager, $entityName, $entityId = false)
    {
        $entityClassName = 'App\\Entity\\' . $entityName;

        if ($entityId === false) {
            // New action
            $entityObject = new $entityClassName;
        } else {
            // Edit action
            $entityObject = $this->getDoctrine()->getRepository($entityClassName)
                ->find($entityId);
        }

        $entityAdmin = $this->globalContainer->get('App\\Admin\\' . $entityName . 'Admin');

        /** @var FormInterface $form */
        $form = $entityAdmin->getForm($entityObject);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityObject = $form->getData();

            $entityManager->persist($entityObject);
            $entityManager->flush();

            return $this->redirectToRoute('admin_entity_edit', [
                'entityName' => $entityName,
                'entityId' => $entityObject->getId()
            ]);
        }

        return $this->render('admin/entity/form.html.twig', [
            'entityObject' => $entityObject,
            'entityName' => $entityName,
            'entityAdmin' => $entityAdmin,
            'form' => $form->createView()
        ]);
    }
}
