<?php

namespace App\Entity;

use App\Entity\Traits\MagicEntity;
use App\Entity\Traits\Timestamps;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 *
 * MagicEntity methods
 * @method string getName
 * @method object setName(string $value)
 */
class User
{
    use MagicEntity, Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;
}
