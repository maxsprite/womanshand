<?php

namespace App\Entity\Traits;

trait MagicEntity
{
    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->$name;
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    /**
     * @param string $method
     * @param mixed $arguments
     *
     * @return mixed
     */
    public function __call($method, $arguments)
    {
        $property = lcfirst(substr($method, 3));

        if (strncasecmp($method, 'get', 3) === 0) {
            return $this->$property;
        }

        if (strncasecmp($method, 'set', 3) === 0) {
            $this->$property = $arguments[0];
        }

        return $this;
    }
}