<?php

namespace App\Admin;

use Symfony\Component\Form\Extension\Core\Type\FormType;

/**
 * Class ProductAdmin
 * @package App\Admin
 */
class ProductAdmin extends EntityAdmin
{
    public function getListFields()
    {
        return [
            'id',
            'title',
            'price',
            'createdAt'
        ];
    }

    public function getForm($entityObject)
    {
        return $this->createForm($entityObject)
            ->add('isActive')
            ->add('title')
            ->add('price')
            ->add('description')
            ->getForm();
    }
}