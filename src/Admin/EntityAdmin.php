<?php

namespace App\Admin;

use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;

/**
 * Class EntityAdmin
 * @package App\Admin
 */
abstract class EntityAdmin
{
    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * EntityAdmin constructor.
     * @param FormFactoryInterface $formFactory
     */
    public function __construct(FormFactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    /**
     * @param $entityObject
     * @return \Symfony\Component\Form\FormBuilderInterface
     */
    public function createForm($entityObject)
    {
        return $this->formFactory->createBuilder(FormType::class, $entityObject);
    }

    /**
     * @return array
     */
    abstract public function getListFields();

    /**
     * @param $entityObject
     * @return FormInterface
     */
    abstract public function getForm($entityObject);
}